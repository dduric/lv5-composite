﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_kompozit
{
    class Program
    {
        static void Main(string[] args)
        {
            Box shipment = new Box("Amazon", 1.2);
            shipment.Add(new Product("Movie", 100, 2.0));

            Box bonus = new Box("Bonus", 1.5);
            bonus.Add(new Product("Toy", 50, 2.5));

            shipment.Add(bonus);


            ShippingService shippingService = new ShippingService(1.99);

            Console.WriteLine(shipment.Description());
            Console.WriteLine(shippingService.calculatePrice(shipment));
        }
    }
}
