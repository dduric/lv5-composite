﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_kompozit
{
    class ShippingService
    {
        private double shippingPrice;

        public ShippingService(double shippingPrice)
        {
            this.shippingPrice = shippingPrice;
        }

        public double calculatePrice(IShipable package)
        {
            return package.Weight * this.shippingPrice;
        }
    }
}
